# Error Handling in C++: Examples and Discussion

This repository contains notes and example code to provide some context for
chosing an error-handling strategy for projects with particular requirements.

- [error-handling.md](error-handling.md) summarizes some
  advantages/disadvantages of various error-handling strategies.
- [error-classification.md](error-classification.md) gives a short introduction
  into various error categories and the impact of the contracts we use on the
  error handling strategy.

## Building and running the tests

To build without running the tests:

```
bazel build //...
```

To run the unit tests (this is the only command you should normally need):

```
bazel test //...
```

or if you want to see the individual test outcomes:

```
bazel test --test_output=all //...
```

## The error handling styles

A few simple example programs to illustrate various sytles of error handling:

- `global-error-code`: The good old `errno`-based error reporting mechanism
  that we all know and, uh, love?

  Each function returns a success indicator and, if the function failed, sets
  `errno` to some predefined value from `cerrno`

  Since functions don't necessarily reset `errno` if they don't cause an error
  the value of `errno` is not a reliable indicator whether the last call caused
  problems unless you check and reset it after each call.

  Note that I am deliberately returning a Boolean "success" value which is
  `true` when the call was successful, not an integer-based "status" value
  (which is typically but not always `0` when the call succeeds) to illustrate
  one of the problems with this approach.

  Also note that the return values of all functions are marked `NODISCARD` so
  that there is at least some incentive to check the return value.

- `error-code-with-query`: Similar to `global-error-code`, but in this case we
  use a custom enumeration to hold the error code. Each function that does not
  fail sets an global error-code variable to indicate success, so we can reliably
  check whether a function succeeded by looking at the error code. (The
  implementation is not thread safe to keep things simple, but we could use a
  thread-local variable to store the error code.)

  Note that in this case we free up the return-value channel for actual
  function results which simplifies the normal use of the functions.

  Note also that this way of handling errors makes it particularly convenient
  to not handle them at all.

- `types-expected`: An example for a type-based way of handling errors. In this
  case I'm using the proposed `std::expected` type, but solutions based on
  `std::optional`, `std::variant` or `boost::outcome` would all look rather
  similar.

  In contrast to the `error-code-with-query` approach we cannot simply ignore
  errors returned by functions if we are interested in the value computed by
  the function. Note, however, that nothing forces us to check the error code
  of functions returning `void`.

- `try-catch`: example for an exception-based solution.

  We use a `throw`-`catch` block in the implementation of `CreateArray` to
  indicate that we are actually prepared to throw *and* *catch* exceptions in
  this solution, even though this particular use of exceptions is a gross abuse
  of the mechanism.

- `throw-only`: example for an exception-based solution in which we don't catch
  exceptions (except perhaps at the topmost level of a module or inside the
  program's main loop).
