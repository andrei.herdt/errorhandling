#include "error-code-with-query.h"

#include <gmock/gmock.h>

using namespace ::testing;

class ErrorCodeWithQuery : public Test
{
public:
	ErrorCodeWithQuery()
	{
		ClearStatus();
	}
};

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, DomainError_SetsErrorCode)
{
	DomainError();
	EXPECT_THAT(GetStatus(), Eq(Status::DomainError));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, OutOfMemoryError_SetsErrorCode)
{
	OutOfMemoryError();
	EXPECT_THAT(GetStatus(), Eq(Status::OutOfMemoryError));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, ClearStatus_ClearsStatus)
{
	DomainError();
	ASSERT_THAT(GetStatus(), Eq(Status::DomainError));

	ClearStatus();
	EXPECT_THAT(GetStatus(), Eq(Status::None));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, ComputeSum_ComputesSumOfArgumentsAndDoesNotSetErrorCode)
{
	EXPECT_THAT(ComputeSum(2.0, 5.0), DoubleEq(7.0));
	EXPECT_THAT(GetStatus(), Eq(Status::None));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, ComputeSqrt_ComputesResultAndDoesNotSetErrorCode_WhenCalledWithZeroArg)
{
	EXPECT_THAT(ComputeSqrt(0.0), DoubleEq(0.0));
	EXPECT_THAT(GetStatus(), Eq(Status::None));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, ComputeSqrt_ComputesResultAndDoesNotSetErrorCode_WhenCalledWithPositiveArg)
{
	EXPECT_THAT(ComputeSqrt(4.0), DoubleEq(2.0));
	EXPECT_THAT(GetStatus(), Eq(Status::None));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, ComputeSqrt_SetsErrorCode_WhenCalledWithNegativeArg)
{
	EXPECT_THAT(ComputeSqrt(-4.0), Eq(0.0));
	EXPECT_THAT(GetStatus(), Eq(Status::DomainError));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, AllocateIntArray_ReturnsArray_WhenArgsAreValid)
{
	std::unique_ptr<double[]> Result{AllocateDoubleArray(10)};
	EXPECT_THAT(Result.get(), Not(Eq(nullptr)));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, AllocateIntArray_SetsErrorCode_WhenArgsAreInvalid)
{
	std::unique_ptr<double[]> Result{AllocateDoubleArray(200)};
	EXPECT_THAT(GetStatus(), Eq(Status::OutOfMemoryError));
}

// NOLINTNEXTLINE
TEST_F(ErrorCodeWithQuery, CreateArray_CreatesArray)
{
	constexpr int Size{20};
	auto Result{CreateArray(Size)};
	int Delta{Size / 2}; // NOLINT

	for (int i{0}; i < Delta; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(Delta - i));
	}

	for (int i{Delta}; i < Size; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(i - Delta));
	}
}
