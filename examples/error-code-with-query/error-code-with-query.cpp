#include "error-code-with-query.h"

#include <cmath>

namespace
{
Status CurrentStatus{Status::None};
}

void DomainError()
{
	CurrentStatus = Status::DomainError;
}

void OutOfMemoryError()
{
	CurrentStatus = Status::OutOfMemoryError;
}

void ClearStatus()
{
	CurrentStatus = Status::None;
}

Status GetStatus()
{
	return CurrentStatus;
}

double ComputeSum(double Lhs, double Rhs) noexcept
{
	return Lhs + Rhs;
}

double ComputeSqrt(double Arg) noexcept
{
	if (Arg < 0)
	{
		DomainError();
		return 0.0;
	}
	return sqrt(Arg);
}

std::unique_ptr<double[]> AllocateDoubleArray(int Size) noexcept
{
	if (Size > 100)
	{
		OutOfMemoryError();
		return std::unique_ptr<double[]>{};
	}

	try
	{
		return std::make_unique<double[]>(static_cast<size_t>(Size));
	}
	catch (std::bad_alloc& e)
	{
		// This is unlikely to work and very difficult to test!
		OutOfMemoryError();
		return std::unique_ptr<double[]>{};
	}
}

std::unique_ptr<double[]> CreateArray(int Size) noexcept
{
	// Don't do this?
	ClearStatus();
	auto Result{AllocateDoubleArray(Size)};

	if (GetStatus() == Status::None)
	{
		int Delta{Size / 2}; // NOLINT
		for (int i = 0; i < Size; ++i)
		{
			auto Value{ComputeSqrt(i - Delta)};

			if (GetStatus() != Status::None)
			{
				ClearStatus();
				Value = ComputeSqrt(Delta - i);
			}
			(Result.get())[i] = round(Value * Value);
		}
	}
	return Result;
}
