#pragma once

#include <memory>

enum class Status
{
	None,
	DomainError,
	OutOfMemoryError
};

void DomainError();
void OutOfMemoryError();

void ClearStatus();
Status GetStatus();

double ComputeSum(double Lhs, double Rhs) noexcept;
double ComputeSqrt(double Arg) noexcept;
std::unique_ptr<double[]> AllocateDoubleArray(int Size) noexcept;
std::unique_ptr<double[]> CreateArray(int Size) noexcept;