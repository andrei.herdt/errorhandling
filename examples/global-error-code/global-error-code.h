#pragma once

#include "attributes.h"
#include <memory>

void DomainError();
void OutOfMemoryError();

NODISCARD bool ComputeSum(double Lhs, double Rhs, double& Result) noexcept;
NODISCARD bool ComputeSqrt(double Arg, double& Result) noexcept;
NODISCARD bool AllocateDoubleArray(int Size, std::unique_ptr<double[]>& Result) noexcept;
NODISCARD bool CreateArray(int Size, std::unique_ptr<double[]>& Result) noexcept;