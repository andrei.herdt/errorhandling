#ifndef ERRORHANDLING_EXPECTED_H
#define ERRORHANDLING_EXPECTED_H

class unexpected
{
public:
	explicit unexpected(const char* msg) noexcept : error_message{msg}
	{
	}
	const char* get_error_message() const noexcept
	{
		return error_message;
	}

private:
	const char* error_message;
};

template <typename T> class expected
{
public:
	expected(T ok_value) noexcept : value{ok_value} {};
	expected(unexpected error) noexcept : error_message{error.get_error_message()} {};

	T get_value() const noexcept
	{
		return value;
	}

	const char* get_error_message() const noexcept
	{
		return error_message;
	}

	bool is_valid() const noexcept
	{
		return error_message == nullptr;
	}

private:
	T value;
	const char* error_message = nullptr;
};

#endif // ERRORHANDLING_EXPECTED_H
