#pragma once

#include <memory>

double ComputeSum(double Lhs, double Rhs) noexcept;
double ComputeSqrt(double Arg);
std::unique_ptr<double[]> AllocateDoubleArray(int Size);
std::unique_ptr<double[]> CreateArray(int Size);
