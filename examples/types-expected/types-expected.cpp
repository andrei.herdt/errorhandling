#include "types-expected.h"
#include <cmath>

using namespace std::string_literals;

tl::expected<double, std::string> ComputeSum(double Lhs, double Rhs) noexcept
{
	return Lhs + Rhs;
}

tl::expected<double, std::string> ComputeSqrt(double Arg) noexcept
{
	if (Arg < 0)
	{
		return tl::make_unexpected("Cannot compute square root of negative number."s);
	}
	return sqrt(Arg);
}

tl::expected<std::unique_ptr<double[]>, std::string> AllocateDoubleArray(int Size) noexcept
{
	if (Size > 100)
	{
		return tl::make_unexpected("Cannot allocate double[] with size > 100."s);
	}

	try
	{
		return std::make_unique<double[]>(Size);
	}
	catch (std::bad_alloc& e)
	{
		// This is unlikely to work and very difficult to test!
		return tl::make_unexpected("Memory allocation failed for double[]."s);
	}
}

tl::expected<std::unique_ptr<double[]>, std::string> CreateArray(int Size)
{
	auto Result{AllocateDoubleArray(Size)};

	if (Result.has_value()) {
		int Delta{Size / 2}; // NOLINT
		for (int i = 0; i < Size; ++i)
		{
			auto Value{ComputeSqrt(i - Delta)};

			if (!Value.has_value())
			{
			  Value = ComputeSqrt(Delta - i);
			}

			Result.value()[i] = round(Value.value() * Value.value());
		}
	}
	return Result;
}